#
# # Exercise_2
# def is_animal_need_vaccination(kind_of_animal, age):
#
#     if not isinstance(kind_of_animal, str):
#         raise TypeError(f"String expected, you got '{type(age).__name__}'")
#     elif kind_of_animal != "cat" or kind_of_animal != "dog":
#         raise ValueError(f"You put {kind_of_animal} value. We consider only cat and dog.")
#     elif not isinstance(age, int):
#         raise TypeError(f"Integer expected, you got '{type(age).__name__}'")
#     elif age < 0:
#         raise ValueError("Age must not be negative!")
#
#     else:
#         if age == 1:
#             return print(f"Yes! Your {kind_of_animal} has {age} and it needs vaccination in first age of live.")
#
#         elif kind_of_animal == "cat" and (age - 1) % 3 == 0:
#             return print(f"Yes! Your {kind_of_animal} has {age} and it needs vaccination every 3 years.")
#
#         elif kind_of_animal == "dog" and (age - 1) % 2 == 0:
#             return print(f"Yes! Your {kind_of_animal} has {age} and it needs vaccination every 2 years.")
#         else:
#             return print(f"Your {kind_of_animal} has {age} and it doesn't need a vaccination.")
#
#
# # is_animal_need_vaccination("dog", -11)
# is_animal_need_vaccination("dog", 1)
# is_animal_need_vaccination("dog", 3)
# is_animal_need_vaccination("dog", 5)
# is_animal_need_vaccination("dog", 2)
# is_animal_need_vaccination("dog", 4)
# print("------------------------------")
# is_animal_need_vaccination("cat", 1)
# is_animal_need_vaccination("cat", 3)
# is_animal_need_vaccination("cat", 5)
# is_animal_need_vaccination("cat", 2)
# is_animal_need_vaccination("cat", 4)
# print("------------------------------")
# is_animal_need_vaccination("tiger", 1)
# is_animal_need_vaccination("elephant", 3)
#
# is_animal_need_vaccination("dog", 4)


# warunek z Adriana Gonciarza z zajęć

allowed_animal_kind = ["cat", "dog"]


def validate_animal_data(age, kind):
    if age < 0:
        raise ValueError("Age must be grater than or equal to zero.")
    elif kind not in allowed_animal_kind:
        raise ValueError("We consider only cat and dog.")


def is_animal_requries_vaccation(age, kind):
    return (kind == "dog" and (age-1) % 2 == 0) or (kind == "cat" and (age-1) % 3 == 0)
    # nie trzeba dawać warunku z age == 1 poniewaz (1-1)%2 == 0


def check_if_animal_requries_vaccation(age, kind):
    validate_animal_data(age, kind)
    return is_animal_requries_vaccation(age,kind)


if __name__ == '__main__':
    print(check_if_animal_requries_vaccation(-1, "cat"))
    print(check_if_animal_requries_vaccation(1, "dog"))
    print(check_if_animal_requries_vaccation(3, "dog"))
    print(check_if_animal_requries_vaccation(7, "cat"))