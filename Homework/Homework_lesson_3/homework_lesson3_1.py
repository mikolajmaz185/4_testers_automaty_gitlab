# Exercise_1

def player_discriptions(player_dict):
    source = player_dict.get("source", "Steam")
    print(f"The player {player_dict['nick']} is of type {player_dict['type']} "
          f"and has {player_dict['exp_points']} EXP.")
    print(f"The source of player {source}.")


player_1 = {
    "nick": "maestro54",
    "type": "warrior",
    "exp_points": 3000
}
player_2 = {
    "nick": "borys54",
    "type": "angel",
    "exp_points": 45000,
    "source": "Playstation"
}

if __name__ == '__main__':
    player_discriptions(player_1)
    player_discriptions(player_2)
