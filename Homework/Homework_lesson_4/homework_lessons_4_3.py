from homework_lessons_4_2 import all_employees


def return_list_with_seniority_grater_than_ten(employees_list):

    seniority_grater_than_ten = []

    for employee in employees_list:

        if int(employee['seniority_years']) > 10:
            print(employee)
            seniority_grater_than_ten.append(employee)

    return seniority_grater_than_ten


list_seniority_greater_than_ten = return_list_with_seniority_grater_than_ten(all_employees)


if __name__ == '__main__':

    print(list_seniority_greater_than_ten)
    print(f"The number of employee withe seniority grater than 10 is: {len(list_seniority_greater_than_ten)}.")
    print(all_employees)
