from datetime import date


def has_warranty(year_car, mileage_car):

    current_year = date.today().year

    if current_year - year_car < 5 and mileage_car < 60000:
        return True
    else:
        return False


if __name__ == '__main__':

    print(has_warranty(2020, 30000))
    print(has_warranty(2020, 70000))
    print(has_warranty(2016, 30000))
    print(has_warranty(2016, 120000))
