import random
from homework_lessons_4_2_data import female_names, men_names, domains, surnames


def generate_random_name():
    random_name = random.choice(female_names + men_names)
    return random_name


def is_female(name):
    if name in female_names:
        return True
    else:
        return False


def generate_random_surname():
    random_surname = random.choice(surnames)
    return random_surname


def generate_random_domain():
    random_domian = random.choice(domains)
    return random_domian


def generate_random_email(name):
    email_address = (name + generate_random_surname() + generate_random_domain()).lower()
    return email_address


def generate_random_seniority_years():
    seniority_years = random.randint(0, 45)

    return seniority_years


def generate_random_employee():

    random_name = generate_random_name()
    is_female_answer = is_female(random_name)
    random_email = generate_random_email(random_name)

    random_employee = {
        "email": random_email,
        "seniority_years": generate_random_seniority_years(),
        "female": is_female_answer
    }
    return random_employee


def generate_list_of_employees(how_many_employees):
    list_of_emoployees = []

    for employee in range(how_many_employees):
        list_of_emoployees.append(generate_random_employee())

    return list_of_emoployees


all_employees = generate_list_of_employees(5)
#if __name__ == '__main__':

