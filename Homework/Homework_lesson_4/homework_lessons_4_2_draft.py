import random
from homework_lessons_4_2_data import female_names, men_names, domains, surnames


def generate_random_name():
    random_name = random.choice(female_names + men_names)
    return random_name


name = generate_random_name()


def generate_random_surname():
    random_surname = random.choice(surnames)
    return random_surname


surname = generate_random_surname()


def generate_random_domain():
    random_domian = random.choice(domains)
    return random_domian


domain = generate_random_domain()


# def generate_random_email():
#     email_address = (generate_random_name() + generate_random_surname() + generate_random_domain()).lower()
#     return email_address

def generate_random_email():
    email_address = (name + surname + domain).lower()
    return email_address


email = generate_random_email()


def generate_random_seniority_years():
    seniority_years = random.randint(0, 45)

    return seniority_years


seniority = generate_random_seniority_years()


def is_female():
    if name in female_names:
        return True
    else:
        return False


def generate_random_employee():
    random_employee = {
        "email": email,
        "seniority_years": seniority,
        "female": is_female()
    }
    return random_employee

def generate_list_of_employees(how_many_employees):
    list_of_emoployees = []

    for employee in range(how_many_employees):
        list_of_emoployees.append(generate_random_employee())

    return list_of_emoployees

if __name__ == '__main__':

    print(generate_random_employee())
    print(generate_list_of_employees(4))
