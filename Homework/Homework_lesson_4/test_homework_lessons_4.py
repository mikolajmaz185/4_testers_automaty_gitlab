import random
from homework_lessons_4_2_data import female_names, men_names, domains, surnames
from homework_lessons_4_2 import (generate_random_name, is_female, generate_random_seniority_years,
                                  generate_random_email)


def test_if_function_generate_random_name():
    """Tests that generate_random_name returns a name from the combined lists."""
    name = generate_random_name()
    assert name in female_names + men_names


def test_is_female():
    """Tests that is_female correctly identifies names from the female list."""
    for name in female_names:
        assert is_female(name) is True

    for name in men_names:
        assert is_female(name) is False


def test_generate_random_surname():
    """Tests that generate_random_surname returns a surname from the list."""
    random_surname = random.choice(surnames)
    assert random_surname in surnames


def test_generate_random_domain():
    """Tests that generate_random_surname returns a surname from the list."""
    random_domian = random.choice(domains)
    assert random_domian in domains


def test_generate_random_email():
    """Tests that generate_random_email creates a valid email format."""
    name = generate_random_name()
    email = generate_random_email(name)

    # Assert email structure (basic validation)
    assert "@" in email
    assert email.count("@") == 1  # Only one "@" symbol
    assert email.count(".") >= 1  # At least one dot for domain


def test_generate_random_seniority_years():
    """Tests that generate_random_seniority_years returns a value between 0 and 45."""
    for _ in range(10):  # Run test multiple times to increase coverage
        years = generate_random_seniority_years()
        assert 0 <= years <= 45
