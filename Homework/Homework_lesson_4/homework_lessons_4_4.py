import random
from datetime import date
from homework_lessons_4_4_data import female_fnames, male_fnames, surnames, countries


def is_adult(person_age):
    if person_age >= 18:
        return True
    else:
        return False


def print_description(new_person, age_int):

    return print(f"Hi! I'm {new_person['firstname']} {new_person['lastname']}. I come from {new_person['country']} and I was born in {age_int}")


def create_dict(list_of_names):

    name = random.choice(list_of_names)
    surname = random.choice(surnames)
    age = random.randint(5,46)
    country = random.choice(countries)
    born_age = date.today().year - age

    client = {
        'firstname': name,
        'lastname': surname,
        'email': f'{name}{surname}.@example.com'.lower(),
        'age': age,
        'country': country,
        'adult': is_adult(age)
    }

    print_description(client, born_age)

    return client


def generate_list_of_ten_people():

    list_of_people = []

    for i in range(10):
        if i in range(0, 5):
            new_person = create_dict(female_fnames)
            list_of_people.append(new_person)
        else:
            new_person = create_dict(male_fnames)
            list_of_people.append(new_person)
    return list_of_people


if __name__ == '__main__':
    print(generate_list_of_ten_people())
