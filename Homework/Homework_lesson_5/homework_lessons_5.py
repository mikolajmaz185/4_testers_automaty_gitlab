# TO DO
"""
Napisz klasę, która opisuje samochód. Każdy samochód ma następujące cechy:
● marka
● kolor
● rocznik
● przebieg
W trakcie tworzenia samochodu trzeba podać jego markę, kolor irocznik.
Przebieg nowego samochodu to 0. Utwórz 3 różne samochody.

drive(distance) -> zwiększa przebieg auta o dystans
get_age() -> zwraca wiek samochodu
repaint(color) -> zmienia kolor samochodu
"""


class NewCar:

    def __init__(self, brand, color, date_production, distance):
        self.brand = brand
        self.color = color
        self.date_production = date_production
        self.distance = distance

    def set_distance(self, new_distance):
        self.distance = new_distance

    def repaint(self, new_color):
        self.color = new_color

    def get_age(self):
        return print(f"It's a date of production: {self.date_production}")


if __name__ == '__main__':

    toyota_auris = NewCar("Toyota", "blue", 2024, 0)
    vw_golf = NewCar("Golf", "silver", 2014, 234560)
    audi_a3 = NewCar("Audi", "black", 2021, 54310)

    print(vw_golf.distance)
    vw_golf.set_distance(289760)
    print(vw_golf.distance)

    vw_golf.get_age()
    toyota_auris.get_age()

    print(audi_a3.color)
    audi_a3.repaint("red")
    print(audi_a3.color)
