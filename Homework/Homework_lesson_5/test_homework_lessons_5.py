from homework_lessons_5 import NewCar


def test_new_car_creation():

    alfa_romeo = NewCar("Alfa Romeo", "red", 2022, 31435)
    assert alfa_romeo.brand == "Alfa Romeo"
    assert alfa_romeo.color == "red"
    assert alfa_romeo.date_production == 2022
    assert alfa_romeo.distance == 31435

    # Test with different data types (year as string)
    ford = NewCar("Ford", "Blue", 2023, 5000)
    assert ford.date_production == 2023


def test_set_distance_valid():
    toyota = NewCar("Toyota", "Black", 2021, 20000)
    toyota.set_distance(30000)
    assert toyota.distance == 30000


def test_repaint():
    bmw = NewCar("BMW", "White", 2019, 40000)
    bmw.repaint("Green")
    assert bmw.color == "Green"


def test_get_age():
    mercedes = NewCar("Mercedes", "Gray", 2018, 60000)
    age = mercedes.get_age()
    assert age == 6
